import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloModule, Apollo } from 'apollo-angular';
import { environment } from 'environments/environment';

@NgModule({
    imports: [
        // ... other modules
        HttpClientModule,
        HttpLinkModule,
        ApolloModule
    ]
})

export class GraphQLModule {
    
    constructor(
        apollo: Apollo,
        httpLink: HttpLink
    ) {
        apollo.create({
            link: httpLink.create({ uri: environment.graphQlUrl + '/graphql' }),
            cache: new InMemoryCache()
        });
    }
}