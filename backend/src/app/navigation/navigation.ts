import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
   
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        icon: 'supervisor_account',
        url: '/dashboard',
        // badge: {
        //     title: '25',
        //     translate: 'NAV.SAMPLE.BADGE',
        //     bg: '#F44336',
        //     fg: '#FFFFFF'
        // }
    },
    {
        id: 'users',
        title: 'Users',
        type: 'item',
        icon: 'supervisor_account',
        url: '/users',
    },
    {
        id: 'roles',
        title: 'Roles',
        type: 'item',
        icon: 'supervisor_account',
        url: '/roles',
    },
    {
        id: 'settings',
        title: 'Settings',
        type: 'collapsable',
        icon: 'settings',
        children: [
            {
                id: 'general_setting',
                title: 'General Settings',
                type: 'item',
                icon: 'settings',
                url: '/settings',
            },
            {
                id: 'email_template',
                title: 'Email Templates',
                type: 'item',
                icon: 'email',
                url: '/email-template',
            },
            
        ]
    },
];
