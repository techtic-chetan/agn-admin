import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject, from } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { UserService } from 'app/shared/services/user.service';
import { UsersUserFormDialogComponent } from '../user-form/user-form.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';

@Component({
    selector: 'users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class UsersComponent implements OnInit {
    dialogRef: any;
    hasSelectedUsers: boolean;
    searchInput: string;
    @ViewChild('dialogContent', { static: false }) dialogContent: TemplateRef<any>;
    @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
    users: any;
    user: any;
    displayedColumns = ['checkbox', 'avatar', 'name', 'email', 'phone', 'jobTitle', 'buttons'];
    selectedUsers: any[];
    checkboxes: {};
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
  

    constructor(
        private userService: UserService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    ) {
    }

  
    ngOnInit(): void {
    }

    dataSource = (request: any) => {
        return from(this.userService.getUsers(request));
    }
  

    newUser(): void {
        this.dialogRef = this._matDialog.open(UsersUserFormDialogComponent, {
            panelClass: ['fix-left',  'user-form-dialog'],
            data: {
                action: 'new',
                user : {}
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                }
            });
    }

    editUser(user): void {
        this.dialogRef = this._matDialog.open(UsersUserFormDialogComponent, {
            panelClass: ['fix-left', 'user-form-dialog'],
            data: {
                user: user,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                    case 'delete':
                        this.deleteUser(user);
                        break;
                }
            });
    }

    deleteUser(user_id): void {
        this.userService.deleteUser(user_id).then(()=>{
            this.datatable.refresh();
        });
    }


    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
