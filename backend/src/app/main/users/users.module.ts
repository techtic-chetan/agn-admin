import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRadioModule  } from '@angular/material/radio';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';

import { UsersComponent } from 'app/main/users/layout/users.component';
import { UsersUserFormDialogComponent } from 'app/main/users/user-form/user-form.component';
import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
    declarations: [
        UsersComponent,
        UsersUserFormDialogComponent,
    ],
    imports: [
        SharedModule,
        UsersRoutingModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        MatRadioModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
    ],

    entryComponents: [
        UsersUserFormDialogComponent
    ]
})
export class UsersModule {
}
