import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject, from } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';

import { RoleService } from '../../shared/services/role.service';
import { RoleFormDialogComponent } from './role-form/role-form.component';

@Component({
    selector: 'roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RolesComponent implements OnInit {
    dialogRef: any;
    searchInput: string;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild('dialogContent', { static: false }) dialogContent: TemplateRef<any>;
    @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  
    constructor(
        private roleService: RoleService,
        private _matDialog: MatDialog
    ) {
    }

  
    ngOnInit(): void {
    }

    dataSource = (request: any) => {
        return from(this.roleService.getRoles(request));
    }
  

    newRole(): void {
        this.dialogRef = this._matDialog.open(RoleFormDialogComponent, {
            panelClass: ['fix-left',  'user-form-dialog'],
            data: {
                action: 'new',
                role : {}
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                }
            });
    }

    editRole(role): void {
        this.dialogRef = this._matDialog.open(RoleFormDialogComponent, {
            panelClass: ['fix-left', 'user-form-dialog'],
            data: {
                role: role,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                    case 'delete':
                        this.deleteRole(role);
                        break;
                }
            });
    }

    deleteRole(role_id): void {
        this.roleService.deleteRole(role_id).then((data)=>{
           this.datatable.refresh();
        }).catch((err) =>{
            console.log(err.errors);
        });
    }


   
}
