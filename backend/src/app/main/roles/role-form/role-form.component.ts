import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { RoleService } from 'app/shared/services/role.service';
import * as _ from 'underscore';

@Component({
    selector: 'role-form-dialog',
    templateUrl: './role-form.component.html',
    styleUrls: ['./role-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class RoleFormDialogComponent {
    action: string;
    role: any;
    userForm: FormGroup;
    dialogTitle: string;
    errorMsg:string;

  
    constructor(
        public roleService: RoleService,
        public matDialogRef: MatDialogRef<RoleFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) _data: any,
    ) {
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Role';
            this.role = Object.assign({}, _data.role);
        }
        else {
            this.dialogTitle = 'New Role';
            this.role = Object.assign(_data.role || {});
        }
    }

  
    submit(form: NgForm): void {
        form.ngSubmit.emit();
    }
      

    addUpdateRole(f:NgForm): void {
        let role = _.pick(this.role, ["id", "name", "slug"]);
        
        if(f.valid){
            this.roleService.addRole(role).then(()=>{
                this.matDialogRef.close('save');
            }).catch((err)=>{
                this.errorMsg = err.message;
                console.log(err);
            });
        }
    }
  
}
