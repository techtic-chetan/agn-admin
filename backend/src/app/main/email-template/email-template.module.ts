import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from 'ng2-ckeditor';
import { EmailTemplateRoutingModule } from './email-template-routing.module';
import { ListComponent } from './list/list.component';
import { SharedModule } from 'app/shared/shared.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    EmailTemplateRoutingModule,
    CKEditorModule,
    FuseSharedModule,
    FuseConfirmDialogModule
  ]
})
export class EmailTemplateModule { }
