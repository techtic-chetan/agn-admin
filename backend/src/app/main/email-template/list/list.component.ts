import { Component, OnInit, ViewChild, ViewEncapsulation, TemplateRef } from '@angular/core';
import { from } from 'rxjs';
import * as _ from 'underscore';
import { NgForm } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { EmailTemplateService } from 'app/shared/services/email-template.service';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { fuseAnimations } from '@fuse/animations';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-email-template',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations 
})
export class ListComponent implements OnInit {
  templateForm:any = {};
  isLoading:boolean = false;
  searchInput:any = "";
  @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  @ViewChild("templateDialog", { static: false }) templateDialog: TemplateRef<any>;

  constructor(
    private emailTemplateService:EmailTemplateService,
    private helper:HelperService,
    private _matDialog: MatDialog
  ) { }


  emailTemplateList = (request) => {
    return from(this.emailTemplateService.get(request));
  }

  ngOnInit() {
  }
  
  newTemplate(email_template, title) {
    this.templateForm = Object.assign({}, email_template);
    this._matDialog.closeAll();
    const dialogRef = this._matDialog.open(this.templateDialog, {
      panelClass: ['fix-left', 'form-dialog'],
      data: {
        template: this.templateForm,
        title: title
      }
    });
  }
  addUpdateTemplate(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    let request = _.pick(this.templateForm, ["_id", "name", "slug", "subject", "body"]);
    if(this.templateForm && this.templateForm._id){
      request._id = this.templateForm._id;
    }
    this.emailTemplateService.add(request).then((data) => {
      this.helper.successMessage(data, `Email Template successfully ${request._id ? 'updated' : 'added'}.`);
      this.completeAction();
    }).catch((error) => {
      this.completeAction();
      this.helper.errorMessage(error, `Error, while ${request._id ? 'updating' : 'adding'} Email Template.`);
    })

  }
  completeAction(){
    this.isLoading = false;
    this.datatable.refresh();
    this._matDialog.closeAll();
  }

  deleteTemplate(id:string) {
    this.emailTemplateService.delete(id).then((data) => {
      this.completeAction();
      this.helper.successMessage(data, "Email Template successfully deleted.");
    }).catch((error) => {
      this.helper.errorMessage(error, "Error, while deleting Email Template.");
    })
  }
  submit(form: NgForm): void {
    form.ngSubmit.emit();
  }

}
