import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { from } from 'rxjs';
import * as _ from 'underscore';
import { NgForm } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { SettingsService } from 'app/shared/services/settings.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-settings',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations 
})
export class ListComponent implements OnInit {
  settings: any = {};
  loading: boolean = false;
  isLoading: boolean = false;
  type: string;
  @ViewChild('f', { static: false }) addEditForm: NgForm;
  acknowlogmessage:any;
  acknowlogmessageType:string;

  constructor(
    private settingsService:SettingsService,
    private helper:HelperService
  ) { 
    this.getSettings();
  }

  ngOnInit() {
  }

  getSettings(){
    this.loading = true;
    this.settingsService.get().then(settings => {
      this.loading = false;
      this.settings = settings;
    }).catch((error) => {
      this.loading = false;
    });
  }

  addUpdateSettings(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    let keys = [];
    for (let key in this.settings) {
      if ((key == 'commissions' || key == 'coaching_commissions' || key == 'stripe_charges') && !this.settings[key]){
        this.settings[key] = 0;
      }
      keys.push({name: key, value: this.settings[key]});
    }
    this.settingsService.add(keys).then((data) => {
      this.helper.successMessage(data);
      this.acknowlogmessage = data;
      this.acknowlogmessageType = "success";
      this.isLoading = false;
    }).catch((error) => {
      this.isLoading = false;
      this.acknowlogmessage = error;
      this.acknowlogmessageType = "danger";
    });
  }

}
