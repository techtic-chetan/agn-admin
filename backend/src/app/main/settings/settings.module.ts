import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { ListComponent } from './list/list.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [
    ListComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    SettingsRoutingModule,
    FuseSharedModule,
    FuseConfirmDialogModule
  ]
})
export class SettingsModule { }
