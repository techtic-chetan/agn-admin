import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { ForgotPasswordComponent } from './forgot-passwrod/forgotPassword.component';
import { ForgotPasswordRoutingModule } from './forgot-passwrod-routing.module';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [
    SharedModule,
    CommonModule,
    ForgotPasswordRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FuseSharedModule
  ]
})
export class ForgotPasswordModule { }
