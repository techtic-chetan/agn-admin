import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forgotPassword.component.html',
  styleUrls: ['./forgotPassword.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {
  forgotPassForm: FormGroup;
  forgotPasswordResponse: any;
  forgotPasswordType: string = 'danger';

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {

  }

  ngOnInit(): void {
    this.forgotPassForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  doForgotPass() {
    if (this.forgotPassForm.valid) {
      let request: any = {
        ...this.forgotPassForm.value,
      }
      this.authService.forgotPassword(request)
        .then((_res) => {
          this.forgotPasswordResponse = _res;
          this.forgotPasswordType = "success";
         // this.router.navigate(['/login']);
        }).catch((err) => {
          this.forgotPasswordResponse = err;
        });
    }
  }
  
}
