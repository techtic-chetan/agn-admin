import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
  declarations: [LayoutComponent],
  imports: [
    SharedModule,
    CommonModule,
    AuthRoutingModule,
    FuseSharedModule,
    ContentModule
  ]
})
export class AuthModule { }
