import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';


const routes: Routes = [
  {
    path: "",
    component : LayoutComponent,
    children: [
      { path: 'login', loadChildren: './login/login.module#LoginModule' },
      { path: 'forgot-password', loadChildren: './forgot-passwrod/forgot-passwrod.module#ForgotPasswordModule' },
      { path: 'set-password/:token', loadChildren: './set-password/setpassword.module#SetPasswordModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
