import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-set-password',
  templateUrl: './setPassword.component.html',
  styleUrls: ['./setPassword.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class setPasswordComponent implements OnInit {
  setPassForm: FormGroup;
  loginResponse: any = {};
  token: any;
  setPasswordResponse:any;
  setPasswordType:String = 'danger';

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private activeRoute: ActivatedRoute
  ) {

  }

  ngOnInit(): void {

    this.activeRoute.params.subscribe(params => {
      if (params.token != undefined && params.token != null) {
        this.token = params.token;
        console.log(this.token);
        
      }
    });
  
    this.setPassForm = this.formBuilder.group({
      password: ['', Validators.required],
      confrim_password: ['', Validators.required]
    });
  }

  doSetPassword() {
    if (this.setPassForm.valid) {
      let request: any = {
        ...this.setPassForm.value,
        token:this.token
      }
      this.authService.setPassword(request)
        .then((_res) => {
          this.router.navigate(['/login']);
        }).catch((err) => {
          console.log(err);
          this.setPasswordResponse = err;
        });
    }
  }
  
}
