import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { setPasswordComponent } from './setpassword/setPassword.component';
import { SetPasswordRoutingModule } from './setpassword-routing.module';
import { SharedModule } from 'app/shared/shared.module';


@NgModule({
  declarations: [setPasswordComponent],
  imports: [
    SharedModule,
    CommonModule,
    SetPasswordRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FuseSharedModule
  ]
})
export class SetPasswordModule { }
