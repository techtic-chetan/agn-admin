import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { setPasswordComponent } from './setpassword/setPassword.component';


const routes: Routes = [
  { path: "", component: setPasswordComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetPasswordRoutingModule { }
