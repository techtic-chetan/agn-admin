import { NgModule } from '@angular/core';
import { FuseSidebarModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { FooterModule } from 'app/layout/components/footer/footer.module';
import { NavbarModule } from 'app/layout/components/navbar/navbar.module';
import { QuickPanelModule } from 'app/layout/components/quick-panel/quick-panel.module';
import { ToolbarModule } from 'app/layout/components/toolbar/toolbar.module';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { ContentModule } from './components/content/content.module';
import { VerticalLayoutComponent } from './layout/layout.component';


@NgModule({
    declarations: [
        VerticalLayoutComponent
    ],
    imports: [
        CommonModule,
        LayoutRoutingModule,
        FuseSharedModule,
        FuseSidebarModule,
        FooterModule,
        NavbarModule,
        QuickPanelModule,
        ToolbarModule,
        ContentModule
    ],
    exports: [
        VerticalLayoutComponent
    ]
})
export class LayoutModule {
}
