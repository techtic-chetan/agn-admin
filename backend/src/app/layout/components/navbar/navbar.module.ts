import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { NavbarComponent } from 'app/layout/components/navbar/navbar.component';
import { NavbarVerticalStyleModule } from './vertical/style.module';
@NgModule({
    declarations: [
        NavbarComponent
    ],
    imports: [
        FuseSharedModule,
        NavbarVerticalStyleModule,
    ],
    exports: [
        NavbarComponent
    ]
})
export class NavbarModule {
}
