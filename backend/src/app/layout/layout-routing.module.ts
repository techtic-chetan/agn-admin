import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerticalLayoutComponent } from './layout/layout.component';

const routes: Routes = [
    {
        path: '',
        component: VerticalLayoutComponent,
        children: [
            {
                path: 'dashboard', loadChildren: 'app/main/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'users', loadChildren: 'app/main/users/users.module#UsersModule'
            },
            {
                path: 'roles', loadChildren: 'app/main/roles/roles.module#RolesModule'
            },
            {
                path: 'profile', loadChildren: 'app/main/profile/profile.module#ProfileModule'
            },
            { 
                path: 'email-template', loadChildren: 'app/main/email-template/email-template.module#EmailTemplateModule' 
            },
            { 
                path: 'settings', loadChildren: 'app/main/settings/settings.module#SettingsModule' 
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
