import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Components } from './components';

import { RouterModule } from '@angular/router';
import { Directives } from './diractive';
import { CustomModules } from './modules';
import { AuthGuard } from './services/auth/auth.guard';
import { MAT_DATE_LOCALE } from "@angular/material";


const Modules = [
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
];


@NgModule({
  imports: [
    CommonModule,
    ...Modules,
    CustomModules
  ],
  declarations: [
    ...Components,
    ...Directives,
  ],
  exports: [
    CustomModules,
    ...Modules,
    ...Components,
    ...Directives
  ],
  entryComponents: [
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ 
         AuthGuard,
        { provide: MAT_DATE_LOCALE, useValue: 'en' },
      ]
    };
  }
}
