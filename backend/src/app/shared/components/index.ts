
import { ControlMessages } from './control-messages';
import { HttpMessage } from './http-message';

const Components = [
    ControlMessages,
    HttpMessage

];

export { Components };
