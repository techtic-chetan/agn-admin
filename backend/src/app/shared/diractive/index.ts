import { RouteTransformerDirective } from "./route-transformer.directive";
import { ImgDirective } from './img.directive';

export const Directives = [
    RouteTransformerDirective,
    ImgDirective
];
