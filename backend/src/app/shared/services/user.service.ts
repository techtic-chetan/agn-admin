import { Injectable } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    onFilterChanged: Subject<any>;
    filterBy: string = 'all';

    constructor(
        private apollo: Apollo,
    ) {
        this.onFilterChanged = new Subject();
    }

    getUsers(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query users($request:UsersDataTableInput){
                        data : users( input : $request){
                            data {
                                _id
                                first_name
                                last_name
                                email
                                user_type
                                password
                                dob
                                profile_pic
                                gender
                                status
                            }meta{
                                from
                                to
                                total
                                per_page
                                current_page
                                last_page
                            }
                        }
                    }
                `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }

    deleteUser(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteUser($id:String!){
                    data : deleteUser(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    addUser(request: any) {

        return this.apollo.mutate({
            mutation: gql`
                mutation addUser($request: UserInput!){
                    data : addUser( input : $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }
    changePassword(request:any){
        return this.apollo.mutate({
            mutation: gql`
                mutation changePassword($old_password: String!,$password:String!){
                    data : changePassword(old_password: $old_password, password: $password){
                        message
                    }
                }
            `,
            variables: {
                ...request
            }

        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
     
    }

}
