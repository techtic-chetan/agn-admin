import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        
        let return_data:any = await new Promise((resolve, reject)=>{
            const token = this.authService.getToken();
            if (token){
                resolve(true);
            }else{
                resolve(false);
            }
        });
       
        if (return_data){
            return return_data;  
        } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        }
       
    }
}