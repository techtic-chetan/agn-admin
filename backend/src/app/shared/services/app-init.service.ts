import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from './auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AppInitService {

    dataStore: {
        settings: any
    }

    private _settings: BehaviorSubject<any>;
    private _sport_types: BehaviorSubject<any>;


    constructor(
        private apollo: Apollo,
        private authService: AuthService
    ) {
        this.dataStore = {
            settings: {}
        }
        this._settings = new BehaviorSubject(this.dataStore.settings);
    }

    public get settings(): any {
        return this.dataStore.settings;
    }

    public get $settings(): Observable<any> {
        return this._settings.asObservable();
    }

    public set settings(value: any) {
        this.dataStore.settings = value;
        localStorage.setItem('settings', JSON.stringify(this.dataStore.settings));
        this._settings.next(Object.assign({}, this.dataStore).settings);
    }


    init() {
        return this.apollo.query({
            query: gql`
        query init{
          data : init{
            settings
          }
        }       
      `
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                this.settings = data.settings;
                return data;
            }).catch((error) => {
                this.settings = {};
                this.authService.user = null;
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

}
