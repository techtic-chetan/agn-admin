import { Injectable } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class RoleService {
   
    constructor(private apollo: Apollo ) { }

    getRoles(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query roles($request:RolesDataTableInput){
                        data : roles( input : $request){
                            data{
                                id
                                name
                                slug
                                created_at
                                updated_at
                            }
                            meta
                            {
                                from
                                to
                                total
                                per_page
                                current_page
                                last_page
                            }
                        }
                    }
                `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }

    deleteRole(id: any) {
       
        return this.apollo.mutate({
            mutation: gql` 
                mutation deleteRole($id:String!){
                    data : deleteRole(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    addRole(request: any) {

        return this.apollo.mutate({
            mutation: gql`
                mutation addRole($request: RoleInput!){
                    data : addRole( input : $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }
    
}
