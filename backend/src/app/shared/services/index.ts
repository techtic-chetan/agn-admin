import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AuthService } from './auth/auth.service';
import { UserService } from './user.service';
import { HelperService } from './helper.service';
import { RoleService } from './role.service';

const Services = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    UserService,
    AuthService,
    HelperService,
    RoleService
];

export { Services };
