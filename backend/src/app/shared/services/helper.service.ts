import { Injectable } from '@angular/core';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ngx-toasty';
import { Router } from '@angular/router';
import { isJsObject } from '../utils';



@Injectable()
export class HelperService {

    constructor(
        public router:Router, 
        public toastyService:ToastyService, 
        public toastyConfig: ToastyConfig,
    ) {
      
    }

    successMessage(data:any, message:any = "Success"){
        console.log(data.message)
        if(data.message != undefined){
            this.addToast(data.message, "Success!", 'success');
        }else{
            this.addToast(message, "Success!", 'success');
        }
    }

    errorMessage(error:any, default_message:any = "Error, Please refresh the page and try again."){
        
        if(error.message == "No JWT present or has expired"){
            this.addToast('You are not login, Please login.', "Oops!", 'error');
            this.router.navigate(['/login']);
            return;
        }
        
        if(error == "" || error==null){
            this.addToast(default_message, "Oops!", 'error');
            return;
        }
       
        let all_errors:any = {};
        let message:any = "";
        
        if(typeof error == "string"){
            this.addToast(error, "Oops!", 'error');
        }else if(error.messages != undefined){
            if(isJsObject(error.messages)){
                all_errors = error.messages;
                message = [];
                Object.keys(all_errors).map((key) => {
                    for(let er of all_errors[key]) {
                          message.push(er);
                    }
                });
                message = message.join('<br>');
            }else{
                message =  error.messages
            }
            this.addToast(message, "Oops!", 'error');
                     
        }else if(error.message != undefined){
            
            if(error.message != undefined || error.message !=null){
                message = error.message;
            }
            this.addToast(message, "Oops!", 'error');

        }else if(error.errors != undefined){
            if(isJsObject(error.errors)){
                all_errors = error.errors;
                message = [];
                Object.keys(all_errors).map((key) => {
                    for(let er of all_errors[key]) {
                          message.push(er);
                    }
                });
                message = message.join('<br>');
            }
            this.addToast(message, "Oops!", 'error');
        }else{
            if(error.error != undefined || error.error !=null)
              this.addToast(error.error, "Oops!", 'error');
            else
              this.addToast(default_message, "Oops!", 'error');
        }

        if(error.code == 404){
            this.router.navigate(['/404']);
        }
    }



    addToast(msg:any, title:any, type:any) {

        var toastOptions:any = {
            title: title,
            msg: msg,
            showClose: true,
            timeout: 5000,
            theme: 'bootstrap',
            onAdd: () => {
                //console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: function() {
                //console.log('Toast ' + toast.id + ' has been removed!');
            },
            onError: function (err) {
                console.log(err);
                //console.log('Toast ' + toast.id + ' has been removed!');
            }
        };

        switch (type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }    
}