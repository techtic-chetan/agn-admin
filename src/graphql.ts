
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export interface DataTableFilters {
    name?: string;
    type?: string;
}

export interface DataTableOrder {
    direction?: string;
    name?: string;
    type?: string;
}

export interface EmailTemplateDataTableInput {
    limit?: number;
    page?: number;
    search?: number;
    filter?: string;
    filter_in?: DataTableFilters[];
    order?: DataTableOrder;
}

export interface EmailTemplateInput {
    _id?: string;
    subject?: string;
    slug?: string;
    name?: string;
    body?: string;
}

export interface LoginInput {
    email: string;
    password: string;
    user_type?: string;
}

export interface PasswordResetInput {
    token: string;
    password: string;
}

export interface RoleInput {
    id?: string;
    name?: string;
    slug?: string;
}

export interface RolesDataTableInput {
    limit?: number;
    page?: number;
    search?: number;
    filter?: string;
    filter_in?: DataTableFilters[];
    order?: DataTableOrder;
}

export interface UserInput {
    _id?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    user_type?: string;
    password?: string;
    dob?: string;
    profile_pic?: string;
    gender?: string;
    status?: string;
}

export interface UsersDataTableInput {
    user_type?: string;
    sport_type_id?: string;
    limit?: number;
    page?: number;
    search?: number;
    filter?: string;
    filter_in?: DataTableFilters[];
    order?: DataTableOrder;
}

export interface AppInit {
    user?: User;
    settings?: JSON;
    sport_types?: JSON;
}

export interface DataTableInput {
    limit?: number;
    page?: number;
    search?: number;
    order_by?: string;
    order?: string;
}

export interface EmailTemplate {
    _id?: string;
    subject?: string;
    slug?: string;
    name?: string;
    body?: string;
    created_at?: Date;
    updated_at?: Date;
}

export interface EmailTemplateList {
    data?: EmailTemplate[];
    meta?: PaginationMeta;
}

export interface LoginResponse {
    status?: number;
    user?: User;
    token?: string;
}

export interface IMutation {
    addEmailTemplate(input: EmailTemplateInput): SuccessResponse | Promise<SuccessResponse>;
    deleteEmailTemplate(id: string): SuccessResponse | Promise<SuccessResponse>;
    addRole(input: RoleInput): SuccessResponse | Promise<SuccessResponse>;
    deleteRole(id?: string): SuccessResponse | Promise<SuccessResponse>;
    saveSetting(key: string, value?: string): SuccessResponse | Promise<SuccessResponse>;
    saveSettings(settings: JSON): JSON | Promise<JSON>;
    register(input: UserInput): LoginResponse | Promise<LoginResponse>;
    login(input: LoginInput): LoginResponse | Promise<LoginResponse>;
    forgotPassword(email: string): SuccessResponse | Promise<SuccessResponse>;
    setPassword(input: PasswordResetInput): SuccessResponse | Promise<SuccessResponse>;
    addUser(input: UserInput): SuccessResponse | Promise<SuccessResponse>;
    deleteUser(id: string): SuccessResponse | Promise<SuccessResponse>;
    changePassword(old_password: string, password: string): SuccessResponse | Promise<SuccessResponse>;
}

export interface PaginationMeta {
    from?: number;
    to?: number;
    total?: number;
    per_page?: number;
    current_page?: number;
    last_page?: number;
}

export interface IQuery {
    init(): AppInit | Promise<AppInit>;
    emailTemplates(input?: EmailTemplateDataTableInput): EmailTemplateList | Promise<EmailTemplateList>;
    emailTemplate(id: string): EmailTemplate | Promise<EmailTemplate>;
    getEmailTemplates(): EmailTemplate[] | Promise<EmailTemplate[]>;
    roles(input?: RolesDataTableInput): RoleList | Promise<RoleList>;
    getSettings(): JSON | Promise<JSON>;
    users(input?: UsersDataTableInput): UserList | Promise<UserList>;
}

export interface Role {
    id?: string;
    name?: string;
    slug?: string;
    created_at?: Date;
    updated_at?: Date;
}

export interface RoleList {
    data?: Role[];
    meta?: PaginationMeta;
}

export interface SuccessResponse {
    status?: string;
    message?: string;
    data?: JSON;
}

export interface User {
    _id?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    user_type?: string;
    password?: string;
    dob?: string;
    profile_pic?: string;
    gender?: string;
    status?: string;
}

export interface UserList {
    data?: User[];
    meta?: PaginationMeta;
}

export type JSON = any;
export type Upload = any;
