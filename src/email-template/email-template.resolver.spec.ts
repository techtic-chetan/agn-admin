import { Test, TestingModule } from '@nestjs/testing';
import { EmailTemplateResolver } from './email-template.resolver';

describe('EmailTemplateResolver', () => {
  let resolver: EmailTemplateResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmailTemplateResolver],
    }).compile();

    resolver = module.get<EmailTemplateResolver>(EmailTemplateResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
