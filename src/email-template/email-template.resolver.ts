import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { EmailTemplate } from './email-template.entity';
import { EmailTemplateService } from './email-template.service';
import { Pagination, PaginationResponse } from '../common/class';

@Resolver('EmailTemplate')
export class EmailTemplateResolver {

    constructor(
        private readonly emailTemplateService: EmailTemplateService,
    ) { }

    @Query(() => EmailTemplate)
    async emailTemplates(@Args('input') input?: any): Promise<PaginationResponse<EmailTemplate>> {
        return this.emailTemplateService.getAll(input);
    }


    @Query(() => EmailTemplate)
    async emailTemplate(@Args('id') id: string) {
        return await this.emailTemplateService.get(id);
    }

    @Mutation(() => EmailTemplate)
    async addEmailTemplate(@Args('input') input: EmailTemplate) {
        return await this.emailTemplateService.createOrUpdate(input).then((data: EmailTemplate) => {
            return {
                data,
                message: `Email Template has been successfully ${(input._id ? 'added' : "updated")}`,
            }
        });
    }

    @Mutation(() => EmailTemplate)
    async deleteEmailTemplate(@Args('id') id: string) {
        return await this.emailTemplateService.delete(id).then((data) => {
            return {
                message: "Email Template has been successfully deleted",
            }
        });
    }

    @Query(() => EmailTemplate)
    async getEmailTemplates() {
        return await this.emailTemplateService.getEmailTemplates();
    }
}
