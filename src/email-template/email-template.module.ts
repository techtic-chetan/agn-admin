import { Module, DynamicModule } from '@nestjs/common';
import { EmailTemplateService } from './email-template.service';
import { EmailTemplateResolver } from './email-template.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailTemplate } from './email-template.entity';

const providers = [
  EmailTemplateService,
  EmailTemplateResolver,
];


@Module({
  imports:[
    TypeOrmModule.forFeature([EmailTemplate]),
  ],
})
export class EmailTemplateModule {
  static forRoot(): DynamicModule {
    return {
      module: EmailTemplateModule,
      providers: providers,
      exports: providers,
    };
  }
}
