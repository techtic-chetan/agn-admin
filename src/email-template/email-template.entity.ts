import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, BeforeInsert } from "typeorm";
import { toSlug } from "../common/utils";

@Entity()
export class EmailTemplate {
    constructor(input?: any) {
        if (input) {
            Object.assign(this, input);
        }
    }
    
    @BeforeInsert()
    beforeInsert() {
        if (this.name) {
            this.slug = toSlug(this.name);
        }
    }
    
    // @BeforeUpdate()
    // beforeUpdate() {
    //     console.log(`BEFORE ENTITY Update: `, this);
    //     if (this){
    //         this.slug = toSlug(this.name);
    //     }
    // }



    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column("varchar", { length: 100, nullable: true })
    name: string;

    @Column("varchar", { length: 100, nullable: true })
    slug: string;

    @Column("varchar", { length: 500, nullable: true })
    subject: string;

    @Column("varchar", { length: 5000, nullable: true })
    body: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

}
