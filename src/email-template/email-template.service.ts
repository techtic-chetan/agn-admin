import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EmailTemplate } from './email-template.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { bindDataTableQuery } from '../common/utils';
import { Pagination } from '../common/class';

@Injectable()
export class EmailTemplateService {
    constructor(
        @InjectRepository(EmailTemplate)
        private readonly emailTemplateRepository: Repository<EmailTemplate>,
    ) { }


    async getAll(input?:any) {

        let query: SelectQueryBuilder<EmailTemplate> = this.emailTemplateRepository.createQueryBuilder()
        let email_templates:any = [];
        if (input){
            bindDataTableQuery(input, query);
            email_templates = await new Pagination<EmailTemplate>(query).paginate(input.limit, input.page);
        }else{
            email_templates = await query.getMany();
        }
        return email_templates;
    }

    async get(id: string): Promise<EmailTemplate> {
        return this.emailTemplateRepository.findOne(id);
    }


    async createOrUpdate(input: EmailTemplate): Promise<EmailTemplate> {
        const email = new EmailTemplate();

        let fields = ['_id', 'subject', 'name', 'slug', 'body'];

        for (let index = 0; index < fields.length; index++) {
            const element = fields[index];
            email[element] = input[element];
        }


        if (input._id) {
            await this.emailTemplateRepository.update(
                { _id: input._id },
                email,
            );
            return this.emailTemplateRepository.findOne(email._id);
        } else {
            return this.emailTemplateRepository.save(email);
        }
    }

    async delete(id: string) {
        let data = await this.emailTemplateRepository.delete({ _id: id });
        return data;
    }

    async getEmailTemplates() {
        let emailTemplates = await this.emailTemplateRepository.find();
        return emailTemplates;
    }
}
