import { Module, DynamicModule } from '@nestjs/common';
import { Services } from './service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailTemplate } from '../email-template/email-template.entity';
import { AuthModule } from '../common/modules/auth';

const providers = [
];

@Module({
    imports: [
        TypeOrmModule.forFeature([
            EmailTemplate
        ]),
        AuthModule
    ],
    exports: [
        ...Services,
        AuthModule
    ],
    providers: [...Services]
})
export class SharedModule {
    static forRoot(): DynamicModule {
        return {
            module: SharedModule,
            providers: providers,
            exports: providers,
        };
    }
}