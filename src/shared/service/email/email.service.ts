import { Injectable } from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import { frontUrl, adminUrl } from '../../../common/utils';
import { EmailTemplate } from '../../../email-template/email-template.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as nunjucks from 'nunjucks';


@Injectable()
export class MailService {
  constructor(
    private readonly mailerService: MailerService,

    @InjectRepository(EmailTemplate) private readonly emailTemplateRepository: Repository<EmailTemplate>,
  ) { }


  getEmailTemplate(slug) {
    return this.emailTemplateRepository.findOne({ where: { slug } })
  }

  public async sendMail(to: string, slug: string, context: any): Promise<any> {
    const template = await this.emailTemplateRepository.findOne({ where: { slug } })

    if (template) {
      const subject = nunjucks.renderString(template.subject, context);
      const body = nunjucks.renderString(template.body, context);
     
      return this.mailerService.sendMail({
        to: to,
        subject: subject,
        template: 'default',
        html: body,
      });
    }
  }

  public sendForgotPasswordNotification(person: any, context: any = {}): Promise<any> {
    let link = '';
    if (person.user_type == 'admin') {
      link = adminUrl(`set-password/${person.password_reset_token}`);
    } else {
      link = frontUrl(`set-password/${person.password_reset_token}`);
    }
    context = {
      link,
      first_name: person.first_name,
      expire_time: '24 Hours',
      ...context,
    };
    return this.sendMail(person.email, 'forgot-password', context);
  }


}