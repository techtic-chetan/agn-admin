import { ConfigService } from '../common/config.service';
import nunjucks = require('nunjucks');

export function registerNjkHelper(env?: any) {
  if (env == undefined) {
    env = new nunjucks.Environment();
  }
  let config: any = ConfigService.getAll();
  env.addGlobal('config', (key: string) => {
    return config[key];
  });
  return env;
}
