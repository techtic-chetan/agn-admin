import {MigrationInterface, QueryRunner, getRepository, Table, TableColumn} from "typeorm";
import { Role } from "../role/role.entity";

export class Roles1576845939495 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.createTable(new Table({
            name: "role",
            columns: [
                new TableColumn({ name: "id", type: "int", length: '11', isPrimary: true, isGenerated: true, generationStrategy: 'increment', isUnique: true }),
                new TableColumn({ name: "name", type: "varchar", length: '50' }),
                new TableColumn({ name: "slug", type: "varchar", length: '50' }),
                new TableColumn({ name: "created_at", type: "datetime" }),
                new TableColumn({ name: "updated_at", type: "datetime" }),
            ]
        }), true);

        const repository = await getRepository(Role);
        await repository.clear();

        const admin_role = new Role();
        admin_role.name = 'Admin';
        admin_role.slug = 'admin';
        admin_role.created_at = new Date();
        await repository.save(admin_role);

        const user_role = new Role();
        user_role.name = 'User';
        user_role.slug = 'user';
        user_role.created_at = new Date();
        await repository.save(user_role);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `roles`");
    }

}
