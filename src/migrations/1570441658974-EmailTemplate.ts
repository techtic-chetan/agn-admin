import { MigrationInterface, QueryRunner, getRepository, Table, TableColumn } from "typeorm";
import { EmailTemplate } from "../email-template/email-template.entity";

export class EmailTemplate1570441658974 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: "email_template",
            columns: [
                new TableColumn({ name: "_id", type: "varchar", length: '36', isPrimary: true, isGenerated: true, generationStrategy: 'uuid', isUnique: true }),
                new TableColumn({ name: "name", type: "varchar", length: '100' }),
                new TableColumn({ name: "slug", type: "varchar", length: '100' }),
                new TableColumn({ name: "subject", type: "varchar", length: '500' }),
                new TableColumn({ name: "body", type: "varchar", isNullable: true, length: '5000' }),
                new TableColumn({ name: "created_at", type: "datetime" }),
                new TableColumn({ name: "updated_at", type: "datetime" }),
            ]
        }), true);

        await queryRunner.query("ALTER TABLE `email_template` CHANGE `created_at` `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `email_template` CHANGE `updated_at` `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");


        const repository = await getRepository(EmailTemplate);

        await repository.clear();

        const forgot_password = new EmailTemplate();
        forgot_password.name = 'Forgot Password';
        forgot_password.slug = 'forgot-password';
        forgot_password.subject = 'Forgot Password';
        forgot_password.body = '<p>Hello {{ first_name }},</p><p> You are receiving this email because we received a password reset request for your account.</p>< p > <a class= "button button-primary" href = "{{ link }}" target = "_blank" > Reset Password < /a></p ><p>This password reset link will expire in {{ expire_time }}.</p>< p > If you did not request a password reset, no further action is required.< /p>';
        forgot_password.created_at = new Date();
        await repository.save(forgot_password);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `email_template`");
    }

}
