import { MigrationInterface, QueryRunner, TableColumn, Table, getRepository } from "typeorm";
import { User } from "../user/user.entity";
import { becrypt, randomInt } from "../common/utils";
import faker from 'faker';
import _ from 'underscore';
import { UserRole } from "src/role/user-role.entity";

export class User1565050596778 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.createTable(new Table({
            name: "user_role",
            columns: [
                new TableColumn({ name: "id", type: "int", length: '11', isPrimary: true, isGenerated: true, generationStrategy: 'increment', isUnique: true }),
                new TableColumn({ name: "role_id", type: "int", length: '11' }),
                new TableColumn({ name: "user_id", type: "varchar", length: '11' }),
                new TableColumn({ name: "created_at", type: "datetime" }),
                new TableColumn({ name: "updated_at", type: "datetime" }),
            ]
        }), true);

        await queryRunner.createTable(new Table({
            name: "user",
            columns: [
                new TableColumn({ name: "_id", type: "varchar", length: '36', isPrimary: true, isGenerated: true, generationStrategy: 'uuid', isUnique: true }),
                new TableColumn({ name: "first_name", type: "varchar", length: '50' }),
                new TableColumn({ name: "last_name", type: "varchar", isNullable: true, length: '50' }),
                new TableColumn({ name: "email", type: "varchar", isNullable: true, length: '100' }),
                new TableColumn({ name: "password", type: "varchar", isNullable: true, length: '100' }),
                new TableColumn({ name: "email_verified_at", type: "datetime", isNullable: true }),
                new TableColumn({ name: "dob", type: "date", isNullable: true }),
                new TableColumn({ name: "gender", type: "varchar", isNullable: true, length: '20' }),
                new TableColumn({ name: "profile_pic", type: "varchar", isNullable: true, length: '100' }),
                new TableColumn({ name: "status", type: "varchar", isNullable: true, length: '20', default: "'active'" }),
                new TableColumn({ name: "password_reset_token", type: "varchar", isNullable: true, length: '200' }),
                new TableColumn({ name: "password_reset_date", type: "datetime", isNullable: true }),
                new TableColumn({ name: "user_type", type: "varchar", isNullable: true, length: '20', default: "'user'" }),
                new TableColumn({ name: "created_at", type: "datetime" }),
                new TableColumn({ name: "updated_at", type: "datetime" }),
            ]
        }), true);


        await queryRunner.query("ALTER TABLE `user` CHANGE `created_at` `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `user` CHANGE `updated_at` `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");

        const repository = await getRepository(User);

        await repository.clear();

        const admin = new User();
        admin.first_name = 'Admin';
        admin.last_name = 'User';
        admin.email = 'admin@gmail.com';
        admin.user_type = 'admin';
        admin.email_verified_at = new Date();
        admin.status = 'active';
        admin.gender = 'male';
        admin.password = becrypt('admin@123');
        const admin_data = await repository.save(admin);

        
        const role_repository = await getRepository(UserRole);
        await role_repository.clear();

        const admin_role = new UserRole();
        admin_role.role_id = 1;
        admin_role.user_id = admin_data._id;
        admin_role.created_at = new Date();
        await role_repository.save(admin_role); 
                
        
        let user = new User();
        user.first_name = 'Test';
        user.last_name = 'User';
        user.email = 'user@gmail.com';
        user.user_type = 'user';
        user.email_verified_at = new Date();
        user.status = 'active';
        //user.dob = faker.date.past();
        user.gender = 'male';
       // user.profile_pic = await downloadFile(faker.image.avatar());
        user.password = becrypt('test@123');
        const user_data = await repository.save(user);
       
        
        const user_role = new UserRole();
        user_role.role_id = 2;
        user_role.user_id = user_data._id;
        user_role.created_at = new Date();
        await role_repository.save(user_role);

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `user`");
    }

}
