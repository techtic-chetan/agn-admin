import {MigrationInterface, QueryRunner, TableColumn, Table, getRepository} from "typeorm";
import { Setting } from "src/setting/setting.entity";

export class setting1577190577767 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: "setting",
            columns: [
                new TableColumn({ name: "_id", type: "varchar", length: '36', isPrimary: true, isGenerated: true, generationStrategy: 'uuid', isUnique: true }),
                new TableColumn({ name: "name", type: "varchar", length: '200' }),
                new TableColumn({ name: "value", type: "varchar", length: '5000', isNullable: true, }),
                new TableColumn({ name: "created_at", type: "datetime" }),
                new TableColumn({ name: "updated_at", type: "datetime" }),
            ]
        }), true)

        await queryRunner.query("ALTER TABLE `setting` CHANGE `created_at` `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `setting` CHANGE `updated_at` `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
    
        const repository = await getRepository(Setting);

        await repository.clear();


        const phone = new Setting();
        phone.name = 'phone';
        phone.value = '9998884400';
        phone.created_at = new Date();
        await repository.save(phone); 

        const fb = new Setting();
        fb.name = 'fb_link';
        fb.value = 'https://www.facebook.com/';
        fb.created_at = new Date();
       await repository.save(fb); 


        const twitter = new Setting();
        twitter.name = 'twitter_link';
        twitter.value = 'https://twitter.com/';
        twitter.created_at = new Date();
        await repository.save(twitter); 

        const stripe = new Setting();
        stripe.name = 'stripe_charges';
        stripe.value = '10';
        stripe.created_at = new Date();
        await repository.save(stripe); 

        const to_email = new Setting();
        to_email.name = 'to_email';
        to_email.value = 'info@admin.com';
        to_email.created_at = new Date();
        await repository.save(to_email); 


        const from_email = new Setting();
        from_email.name = 'from_email';
        from_email.value = 'info@admin.com';
        from_email.created_at = new Date();
        await repository.save(from_email); 

        const insta = new Setting();
        insta.name = 'insta_link';
        insta.value = 'info@admin.com';
        insta.created_at = new Date();
        await repository.save(insta); 

        const address = new Setting();
        address.name = 'address';
        address.value = 'info@admin.com';
        address.created_at = new Date();
        await repository.save(address); 
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `setting`");
    }

}

