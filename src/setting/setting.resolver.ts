import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { Setting } from './setting.entity';
import { SettingsService } from './setting.service';

@Resolver('Setting')
export class SettingResolver {
    constructor(
        private readonly settingService: SettingsService,
    ) { }

    @Mutation(() => Setting)
    async saveSetting(@Args('key') name: string, @Args('value') value: any) {
        console.log(name,value);
        
        return await this.settingService.saveSettings({name, value}).then((data: Setting) => {
            return {
                data,
                message: `Setting has been successfully saved.`,
            }
        });
    }

    @Mutation(() => Setting)
    async saveSettings(@Args('settings') settings: any) {
        return await this.settingService.saveSettings(settings).then((data: Setting[]) => {
            return {
                data,
                message: `Settings has been successfully saved.`,
            }
        });
    }

    @Query(() => Setting)
    async getSettings() {
       return await this.settingService.getSettings();
    }

}
