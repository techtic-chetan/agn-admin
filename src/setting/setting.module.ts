import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Setting } from './setting.entity';
import { SettingResolver } from './setting.resolver';
import { SharedModule } from '../shared/shared.module';
import { SettingsService } from './setting.service';

const providers = [
  SettingsService,
  SettingResolver,
];

@Module({
  imports: [
    TypeOrmModule.forFeature([Setting]),
    SharedModule
  ],
  providers: providers,
  exports: providers
})
export class SettingsModule {}
