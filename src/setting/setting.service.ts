import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Setting } from './setting.entity';


@Injectable()
export class SettingsService {
    constructor(
        @InjectRepository(Setting)
        private readonly settingRepository: Repository<Setting>,
    ) { }

    async saveSettings(settings: any) {
        console.log('settings',settings);
        
        await settings.forEach((item) => {
            this.settingRepository.delete({ name: item.name });
        });

        return await this.settingRepository.save(settings);
    }

    async getSettings() {
        let settings = await this.settingRepository.find();
        let all_settings: any = {};
        settings.forEach((setting: Setting) => {
            all_settings[setting.name] = setting.value;
        })
        return all_settings;
    }

}
