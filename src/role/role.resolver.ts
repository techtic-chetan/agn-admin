import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { PaginationResponse } from '../common/class';
import * as _ from "underscore";
import { RoleService } from './role.service';
import { Role } from './role.entity';
import { UserInputError } from 'apollo-server-core';


@Resolver('Role')
export class RoleResolver {
    constructor(
        private readonly roleService: RoleService,
    ) { }

    
    @Query(() => Role)
    async roles(@Args('input') input?: any): Promise<PaginationResponse<Role>> {
        return this.roleService.getRoles(input);
    }
  
    @Mutation(() => Role)
    async addRole(@Args('input') input: any) {
        return await this.roleService.createOrUpdate(input).then((role: Role) => {
            if(role){
                return {
                    role,
                    message: `Role has been successfully ${(!input.name) ? 'added' : 'updated'}`,
                }
            }else{
                throw new UserInputError('Already exist role.');
            }
            
        });
    }

    @Mutation(() => Role)
    async deleteRole(@Args('id') id):Promise<any> {
        return await this.roleService.delete(id).then((data) => {
            return {
                message: "Role has been successfully deleted",
            }
        });
    }
           
}
