import { Injectable, Inject, forwardRef, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { Repository } from 'typeorm';
import { AuthenticationError } from 'apollo-server-core';
import { bindDataTableQuery} from '../common/utils';
import { Pagination } from '../common/class';
import { _ } from "underscore";


@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
  ) { }

  async getRoles(input) {

    let query: any = this.roleRepository.createQueryBuilder()
    let roles;
    if (input && input.limit) {
      bindDataTableQuery(input, query);
      roles = new Pagination<Role>(query).paginate(input.limit, input.page);
    } else {
      roles = this.roleRepository.find();
      roles = {
        data: roles
      }
    }
    return roles;
  }
 
  async createOrUpdate(input): Promise<Role> {
    const role = new Role();

    if (input.id) {
      role.id = input.id;
    }

    if (input.slug) {
      let find = await this.roleRepository.findOne({ slug: input.slug });
     
      if (find && find.id != role.id) {
        throw new AuthenticationError('This role is already exist.');
      }
    }

    if (input.name) {
      role.name = input.name;
    }

    if (input.slug) {
      role.slug = input.slug;
    }
   
    if (input.id) {
      await this.roleRepository.update(
        { id: input.id },
        role,
      );
      return this.roleRepository.findOne(role.id);
    } else {
      return this.roleRepository.save(role);
    }
  }

  async delete(delete_id) {
    let data = await this.roleRepository.delete(delete_id);
    return data;
  }
      
}
