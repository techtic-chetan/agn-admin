import { Module, DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { RoleResolver } from './role.resolver';
import { RoleService } from './role.service';
import { SharedModule } from '../shared/shared.module';


const providers = [
  RoleResolver,
  RoleService
];

@Module({
  imports: [
    SharedModule,
    TypeOrmModule.forFeature([Role])
  ],
  providers: providers,
  exports: providers,
})
export class RoleModule {
  static forRoot(): DynamicModule {
    return {
      module: RoleModule,
      providers: [],
      exports: [],
    };
  }
}
