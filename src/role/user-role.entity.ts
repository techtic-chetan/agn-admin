import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class UserRole {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    role_id: number;

    @Column()
    user_id: String;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

}
