import {  Entity, Column, CreateDateColumn, UpdateDateColumn,  PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Role {
  
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 40, nullable: true })
  name: string;

  @Column("varchar", { length: 40, nullable: true })
  slug: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
 
}
