import 'reflect-metadata';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { join } from 'path';
import { ValidationExceptionFilter } from './common/Exception/validation.exception.filter';
import { ValidationPipe } from '@nestjs/common';
import bodyParser = require('body-parser');
declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  // let env = nunjucks.configure('./views', {
  //   express: app,
  //   autoescape: true,
  // });
  // registerNjkHelper(env);
  //app.set('view engine', 'html');

/*   app.useStaticAssets(join(__dirname, '..', 'backend/dist/browser'), { prefix: '/admin' });
  app.useStaticAssets(join(__dirname, '..', 'public'), { prefix: '/public' });
  app.useStaticAssets(join(__dirname, '..', 'frontend/dist/browser')); */

 /*  app.setBaseViewsDir(join(__dirname, '..', 'views')); */
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new ValidationExceptionFilter());

  app.use(bodyParser.json({ limit: '50mb' }));

  app.enableCors();
  app.setGlobalPrefix('api');

  await app
    .listen(3000)
    .then(() => console.log('Server listening on 3000 port'));

  /* if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  } */
}
bootstrap();
