import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User } from './user.entity';
import { AuthService } from '../common/modules/auth/auth.service';
import { PaginationResponse } from '../common/class';
import { ApiAuthGuard, CurrentUser } from '../common/modules/auth';
import { UserInput } from './class/user.input';
import { LoginInput } from './class/login.input';
import { UserInputError } from 'apollo-server-core';
import { PasswordResetInput } from './class/password-reset.input';
import { MailService } from '../shared/service/email/email.service';
import { UseGuards } from '@nestjs/common';


@Resolver('User')
export class UserResolver {
    constructor(
        private readonly userService: UserService,
        private readonly authService: AuthService,
        private mailService: MailService
    ) { }

    
    @Query(() => User)
    async users(@Args('input') input?: any): Promise<PaginationResponse<User>> {
        return this.userService.getUsers(input);
    }
   

    @Mutation(() => User)
    async register(@Args('input') input: UserInput) {
        return await this.userService.createOrUpdate(input).then((user: User) => {
           // let token: string = this.authService.fromUser(user);
            return {
                user,
                //token
            }
        });
    }


    @Mutation(() => User)
    async login(@Args('input') input: LoginInput) {
        return await this.userService.auth(input).then((user: User) => {
            if (user) {
               let token: string = this.authService.fromUser(user);
                return {
                   user,
                   token
                }
            } else {
                throw new UserInputError('Invalid email or password. Please try again.');
            }
        })
    }

    @Mutation(() => User)
    async forgotPassword(@Args('email') email: string) {
        return await this.userService.forgotPassword(email).then(async (user: User | Boolean) => {
            if (user instanceof User) {
                await this.mailService.sendForgotPasswordNotification(user);
                return {
                    message: "Password reset link has been sent on your email, Please check your inbox.",
                }
            } else {
                throw new UserInputError('You are not register with the email. Please try again.');
            }
        })
    }

    @Mutation(() => User)
    async setPassword(@Args('input') input: PasswordResetInput) {
        return await this.userService.setPassword(input).then(() => {
            return {
                message: "Your new password has been set successfully.",
            }
        });
    }

   
    @Mutation(() => User)
    async addUser(@Args('input') input: UserInput) {
        return await this.userService.createOrUpdate(input).then((user: User) => {
            return {
                user,
                message: `User has been successfully ${(!input._id) ? 'added' : 'updated'}`,
            }
        });
    }

    @Mutation(() => User)
    async deleteUser(@Args('id') id: string) {
        return await this.userService.delete(id).then(() => {
            return {
                message: "User has been successfully deleted",
            }
        });
    }

    @Mutation()
    @UseGuards(ApiAuthGuard)
    async changePassword(@CurrentUser() user: User, @Args('old_password') old_password: UserInput, @Args('password') password: UserInput) {
        return await this.userService.changePassword({ old_password, password }, user._id).then((data) => {
            return {
                message: 'Your password has been successfully changed'
            }
        });
    }

   /* @Mutation(() => User)
    async updateProfile(     
        @Args('input') input: UserInput
    ) {
        input._id = user._id;

        return await this.userService.createOrUpdate(input).then((user: User) => {
            return {
                user,
                message: `User has been successfully ${(input._id ? 'updated' : "added")}`,
            }
        });
    }*/

}
