import { Injectable, Inject, forwardRef, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository, In, createQueryBuilder } from 'typeorm';
import { LoginInput } from './class/login.input';
import { UserInput } from './class/user.input';
import { SocialUserInput } from './class/social-user.input';
import { PasswordResetInput } from './class/password-reset.input';
import { AuthenticationError } from 'apollo-server-core';
import moment = require('moment');
import { becrypt, bindDataTableQuery, saveBase64Image } from '../common/utils';

import { Pagination } from '../common/class';
import { _ } from "underscore";
import { UserRole } from '../role/user-role.entity';


@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(UserRole)
    private readonly userRoleRepository: Repository<UserRole>,
    
  ) { }

  async getUsers(input) {
    let where: any = {};

    let query: any = this.userRepository.createQueryBuilder()
    if (input && input.user_type) {
      query = query.where("user_type = :user_type", { user_type: input.user_type })
    }
    let users;
    if (input && input.limit) {
      bindDataTableQuery(input, query);
      users = new Pagination<User>(query).paginate(input.limit, input.page);
    } else {
      users = this.userRepository.find();
      users = {
        data: users
      }
    }

    return users;
 
  }

  async getUser(id: string): Promise<User> {
    return this.userRepository.findOne(id);
  }


  async getUserByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email });
  }

  async auth(input: LoginInput): Promise<User> {
    /* let user_type: any = {};
    if (input.user_type && input.user_type == 'admin') {
      user_type = 'admin';
    } else if (input.user_type && input.user_type == 'player') {
      user_type = 'player';
    } else {
      user_type = In([null, 'user', 'player']);
    } */

    let find: any = {
      email: input.email,
      password: becrypt(input.password),
      user_type: input.user_type,
    };
    return this.userRepository.findOne({ where: { ...find } });
  }

  async createOrUpdate(input): Promise<User> {
  
    const user = new User();

    if (input._id) {
      user._id = input._id;
    }

    if (input.email) {
      user.email = input.email;
      let find = await this.userRepository.findOne({ email: user.email });
      if (find && find._id != user._id) {
        throw new AuthenticationError('This email is already used.');
      }
    }

    if (input.first_name) {
      user.first_name = input.first_name;
    }

    if (input.last_name) {
      user.last_name = input.last_name;
    }

    if (input.user_type) {
      user.user_type = input.user_type;
    }

    if (input.password) {
      user.password = becrypt(input.password);
    }

    if (input.status) {
      user.status = input.status;
    }

    if (input.gender) {
      user.gender = input.gender;
    }

    if (input.profile_pic) {
      const path = saveBase64Image(input.profile_pic);
      if (path){
        user.profile_pic = path
      }
    }

    if (input.dob) {
      user.dob = new Date(input.dob);
    }

    if (input._id) {
      await this.userRepository.update(
        { _id: input._id },
        user,
      );
      return this.userRepository.findOne(user._id);
    } else {
      let user_data = await this.userRepository.save(user);
      let role = await this.userRoleRepository.findOne({ where: { slug: input.user_type } });
      if (role.role_id){
        const user_role = new UserRole();
        user_role.role_id = role.role_id;
        user_role.user_id = user_data._id;
        user_role.created_at = new Date();
        await this.userRoleRepository.save(user_role);
      }
      return user_data;
    }
  }

  async forgotPassword(email: string): Promise<User | Boolean> {
    let user = await this.userRepository.findOne({ email });
    if (user) {
      let token = becrypt(user + '-' + new Date().getTime());
      await this.userRepository.update(
        { _id: user._id },
        {
          password_reset_token: token,
          password_reset_date: new Date(),
        }
      );
      let userData = await this.userRepository.findOne({ email });
      return userData;
    } else {
      return false;
    }
    return true;
  }

  async setPassword(input: PasswordResetInput) {
    const user = await this.userRepository.findOne({
      where: { 'password_reset_token': input.token },
    });

    if (user) {
      if (user.password_reset_token && moment().diff(user.password_reset_date, 'hours') < 24) {
        await this.userRepository.update(
          { _id: user._id },
          {
            password: becrypt(input.password),
            password_reset_token: null,
            password_reset_date: null
          },
        );
      } else {
        throw new AuthenticationError(
          'Your set password link is expired, Please try again forgot password.',
        );
      }
    } else {
      throw new AuthenticationError(
        'Token is not valid, Please try again forgot password.',
      );
    }
  }

  async getCurrentUsers(token: string): Promise<User> {
    try {
     /*  let payload: any = this.authService.verify(token);
      if (payload) {
        return this.userRepository.findOne(payload._id);
      } else {
        return null
      } */
    } catch (error) {
      return null
    }

  }

  async socialLogin(input: SocialUserInput): Promise<User> {
    let userInfo = await this.userRepository.findOne({ email: input.email });
    if (userInfo) {
      return userInfo;
    } else {
      const user = new User();
      user.first_name = input.name;
      user.email = input.email;
      user.password = becrypt(Math.random().toString(36).substring(7));
      user.email_verified_at = new Date();
      user.status = 'active';
      user.user_type = input.user_type;
      return this.userRepository.save(user);
    }
  }

  async delete(id: string) {
    let data = await this.userRepository.delete({ _id: id });
    return data;
  }
  
  async changePassword(input: any, user_id: any) {
    const user = await this.userRepository.findOne(user_id)
    if (user.password == becrypt(input.old_password)){
      console.log('working');
      
      await this.userRepository.update(
        { _id: user_id },
        {
          password: becrypt(input.password),
        }
      );
    }else{
      throw new AuthenticationError( 'Your old password is not correct, Please try again with correct password.' );
    }

  }
 
}
