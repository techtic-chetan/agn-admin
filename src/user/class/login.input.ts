export class LoginInput {
  email: string;
  password: string;
  user_type?: string;
}
