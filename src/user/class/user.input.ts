export class UserInput {
  _id?: string;
  first_name?: string;
  last_name?: string;
  email?: string;
  password?: string;
  dob: Date;
  gender: string;
  status: string;
  profile_pic: string;
  user_type: 'user' | 'admin' | 'player';
}
