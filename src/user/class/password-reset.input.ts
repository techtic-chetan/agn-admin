export class PasswordResetInput {
  token: string;
  password: string;
}
