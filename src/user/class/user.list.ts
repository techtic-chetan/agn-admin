import { User } from '../user.entity';

export class UserList {
  count?: number;
  data?: [User];
}
