import { User } from '../user.entity';

export class ErrorResponse {
  status?: number;
  message: String;
}
