export class SocialUserInput {
  provider?: string;
  token?: string;
  email?: string;
  name?: string;
  user_type?: 'user' | 'admin' | 'player';
}
