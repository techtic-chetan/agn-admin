import { User } from '../user.entity';

export class LoginResponse {
  status?: number;
  user?: User;
  token?: string;
}
