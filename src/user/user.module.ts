import { Module, DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';
import { SharedModule } from '../shared/shared.module';
import { UserRole } from '../role/user-role.entity';


const providers = [
  UserResolver,
  UserService
];

@Module({
  imports: [
    SharedModule,
    TypeOrmModule.forFeature([User, UserRole])
  ],
  providers: providers,
  exports: providers,
})
export class UserModule {
  static forRoot(): DynamicModule {
    return {
      module: UserModule,
      providers: providers,
      exports: providers,
    };
  }
}
