import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  AfterLoad,
} from 'typeorm';
import { baseUrl } from '../common/utils';


@Entity()
export class User {
  constructor(input?: any) {
    if (input) {
      Object.assign(this, input);
    }
  }

  @AfterLoad()
  afterLoad() {
    this.profile_pic = (this.profile_pic) ? baseUrl(this.profile_pic) : '';
  }

  @PrimaryGeneratedColumn('uuid')
  _id: string;

  @Column("varchar", { length: 40, nullable: true })
  first_name: string;

  @Column("varchar", { length: 40, nullable: true })
  last_name: string;

  @Column("varchar", { length: 100, nullable: true })
  email: string;

  @Column("varchar", { length: 200, nullable: true })
  password: string;

  @Column({ type: "datetime", default: null, nullable: true })
  email_verified_at: Date;

  @Column({ type: "date", default: null, nullable: true })
  dob: Date;

  @Column({ type: "varchar", length: 20, default: null, nullable: true })
  gender: string;

  @Column("varchar", { length: 200, nullable: true })
  profile_pic: string;

  @Column("varchar", { length: 20, nullable: true })
  status: string;

  @Column("varchar", { nullable: true })
  password_reset_token: string;

  @Column("varchar", { nullable: true })
  password_reset_date: Date;
  
  @Column('enum', { enum: ["user", "admin", "player"], default: "user" })
  user_type: "user" | "admin" | "player";

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
 
}
