import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from './ormconfig';

import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { Directives } from './common/modules/graph-ql/directives';
import { SharedModule } from './shared/shared.module';
import { EmailTemplateModule } from './email-template/email-template.module';
import { MailModule } from './common/modules/mail/mail.module';
import * as GraphQLJSON from 'graphql-type-json';
import { AuthModule ,jwtConstants } from './common/modules/auth';
import { JwtPayload } from 'passport-jwt';
import * as jwt from 'jsonwebtoken';

import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { SettingsModule } from './setting/setting.module';



@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      definitions: {
        path: join(process.cwd(), 'src/graphql.ts'),
      },
      context: async (data) => {
        let { req, context, connection } = data
        if (connection) {
          let ctx = connection.context
          const jwtToken = ctx.authToken;
          let data = jwt.verify(jwtToken, jwtConstants.secret) as JwtPayload;
          return { ...connection, context: { ...context, user: data, jwtToken } };
        }

        let jwtToken = req.headers['authorization'];
        return {
          ...context,
          req,
          headers: req ? req.headers : {},
          jwtToken,
        };
      },
      installSubscriptionHandlers: true,
      schemaDirectives: Directives,
      introspection: true,
      resolvers: { JSON: GraphQLJSON },
      debug: true,
      playground: true,
    }),
    AuthModule.forRoot(),
    MailModule,
    //GraphQlModule,
    TypeOrmModule.forRoot(ormconfig),
    UserModule.forRoot(),
    RoleModule.forRoot(),
    SharedModule.forRoot(),
    EmailTemplateModule.forRoot(),
    SettingsModule
  
  ],
  exports: [
    UserModule,
    EmailTemplateModule,
    SettingsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }