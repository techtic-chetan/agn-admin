import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {
    private static envConfig: { [key: string]: string } = dotenv.parse(fs.readFileSync('.env'));



    static init(filePath: string) {
        ConfigService.envConfig = dotenv.parse(fs.readFileSync(filePath));
        return ConfigService;
    }

    static get(key: string): any {
        return ConfigService.envConfig[key];
    }

    static getAll(): any {
        return ConfigService.envConfig;
    }
}
